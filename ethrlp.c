#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "ethrlp.h"

int rlp_write_uint64(unsigned char *buf, uint64_t n) {
    unsigned char ibuf[sizeof(n)];
    unsigned char *pibuf = ibuf;
    int i, ibuflen;

    if (n < 128) {
        *buf = (unsigned char)n;
        return 1;
    }
    
    ibuf[0] = (n >> 56) & 0xff;
    ibuf[1] = (n >> 48) & 0xff;
    ibuf[2] = (n >> 40) & 0xff;
    ibuf[3] = (n >> 32) & 0xff;
    ibuf[4] = (n >> 24) & 0xff;
    ibuf[5] = (n >> 16) & 0xff;
    ibuf[6] = (n >> 8) & 0xff;
    ibuf[7] = n & 0xff;

    for (i = 0; i < 7; i++) {
        if (*pibuf != 0)
            break;
        pibuf++;
    }
    ibuflen = 8 - (pibuf - ibuf);

    *buf++ = 0x80 + ibuflen;
    for (i = 0; i < ibuflen; i++) {
        *buf++ = *pibuf++;
    }
    return ibuflen + 1;
}

int rlp_write_string(unsigned char *buf, unsigned char *b, int len) {
    int i, l;
    if (len <= 55) {
        *buf++ = 0x80 + len;
        for (i = 0; i < len; i++) {
            *buf++ = *b++;
        }
        return len + 1;
    }
    l = rlp_write_uint64(buf, len);
    *buf = 0xb7 + l - 1;
    buf += l;
    for (i = 0; i < len; i++) {
        *buf++ = *b++;
    }
    return l + len;
}

int rlp_write_list_uint64(unsigned char *buf, uint64_t *list, int len) {
    int i, l;
    unsigned char *xbuf, *ori_xbuf;
    int xlen;

    xbuf = ori_xbuf = malloc(len * sizeof(uint64_t));
    for (i = 0; i < len; i++) {
        xbuf += rlp_write_uint64(xbuf, list[i]);
    }
    xlen = xbuf - ori_xbuf;

    if (xlen <= 55) {
        *buf++ = 0xc0 + xlen;
        memcpy(buf, xbuf, xlen);
        free(xbuf);
        return xlen + 1;
    } 
    l = rlp_write_uint64(buf, xlen);
    *buf = 0xf7 + l - 1;
    buf += l;
    memcpy(buf, xbuf, xlen);
    free(xbuf);
    return xlen + l;
}

int rlp_write_list_string(unsigned char *buf, unsigned char **list, int len) {
    
}