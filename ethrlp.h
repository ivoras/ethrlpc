#ifndef _ETHRLP_C_
#define _ETHRLP_C_

#include <stdint.h>

int rlp_write_uint64(unsigned char *buf, uint64_t i);
int rlp_write_string(unsigned char *buf, unsigned char *b, int len);
int rlp_write_list_uint64(unsigned char *buf, uint64_t *list, int len);
int rlp_write_list_string(unsigned char *buf, unsigned char **list, int len);

#endif
