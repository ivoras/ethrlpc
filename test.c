#include <stdio.h>

#include "ethrlp.h"

void dump_buf(unsigned char *buf, int l) {
    int i;

    for (i = 0; i < l; i++) {
        printf("%02X ", buf[i]);
    }
    printf("\n");
}

int main() {
    unsigned char buf[100];
    int l;

    l = rlp_write_uint64(buf, 1024);
    dump_buf(buf, l);

    l = rlp_write_string(buf, (unsigned char*)"dog", 3);
    dump_buf(buf, l);


}