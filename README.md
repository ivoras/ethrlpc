# Ethereum LRP library in C

This is a simple C library for the LRP encoding as defined in Ethereum. See https://github.com/ethereum/wiki/wiki/RLP for details.

The exported functions are declared in `ethlrp.h`. They should be pretty obvious to use.
